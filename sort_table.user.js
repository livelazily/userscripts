// ==UserScript==
// @name         sort table
// @namespace    com.livelaziy.reordertable
// @version      0.3.1
// @description  sort table
// @author       livelazily
// @match        http://www.piaotian.com/*
// @match        https://www.piaotian.com/*
// @match        http://www.ptwxz.com/*
// @match        https://www.ptwxz.com/*
// @grant        none
// ==/UserScript==

(($) => {
    'use strict';
    document.ondblclick = null;
    if (!$) {
        return;
    }
    const pathname = location.pathname;
    if (pathname.startsWith('/bookinfo')) {
        return;
    }
    if (!(pathname.startsWith('/book') || pathname.startsWith('/quanben'))) {
        return;
    }

    $(() => {
        const $grid = $('.grid');
        if (!$grid.length) {
            return;
        }
        bindKeyEvents();

        const $trs = $grid.find('tr');
        const rows = $trs.toArray();
        sortRowByWordCount(rows);

        const $tbody = $grid.find('tbody');
        const fragment = document.createDocumentFragment();
        const authors = {};
        $.each(rows, (i, row) => {
            const $tr = $(row);
            const $children = $tr.children();
            const $title = $children.eq(0);
            const $author = $children.eq(2);
            const $size = $children.eq(3);
            const size = parseInt($size.text());
            if (size < 4000) {
                $tr.addClass('lightgrey');
            }
            $size.insertAfter($title);
            $author.insertAfter($size);

            fragment.appendChild(row);

            if (i === 0) {
                // skip head line
                return;
            }
            authors[$author.text()] = $author;
        });
        $tbody.append(fragment);


    });

    function bindKeyEvents() {
        $(document).bind('keydown', (e) => {
            const origEvent = e.originalEvent;
            const key = origEvent.key
            if (key === 'ArrowRight') {
                $('.pagelink .next')[0].click();
            } else if (key === 'ArrowLeft') {
                $('.pagelink .prev')[0].click();
            }
        });
    }

    function sortRowByWordCount(rows) {
        const titleRow = rows.shift();
        rows.sort((row1, row2) => {
            const row1Size = parseInt($(row1).children().eq(3).text());
            const row2Size = parseInt($(row2).children().eq(3).text());
            const result = row2Size - row1Size;
            return result;
        });
        rows.unshift(titleRow);
    }

    function appendAuthorLink(authors) {
        const authorNameArr = Object.keys(authors);
        const authorNames = authorNameArr.join('---');
        urlencode(authorNames, 'gbk', (encodeNames) => {
            const encodeArr = encodeNames.split('---');
            const names = {};
            $.each(encodeArr, (index, encodeName) => {
                names[authorNameArr[index]] = encodeName;
            });

            $.each(authors, (authorName, $author) => {
                const encodeName = names[authorName];
                $author.html(`<a target="_blank" href="http://www.piaotian.com/author/${encodeName}.html">${authorName}</a>`);
            });
        });
    }

    function urlencode(str, charset, callback) {
        const form = getForm(str, charset);
        const iframe = getIframe();

        window.addEventListener('message', messageCb);
        document.body.appendChild(iframe);

        //设置回调编码页面的地址，这里需要用户修改
        form.action = 'https://livelazily.tk/getEncodeStr.html';
        form.submit();

        function getForm(str, charset) {
            //创建form通过accept-charset做encode
            const form = document.createElement('form');
            form.method = 'get';
            form.style.display = 'none';
            form.acceptCharset = charset;

            const input = document.createElement('input');
            input.type = 'hidden';
            input.name = 'str';
            input.value = str;
            form.appendChild(input);
            form.target = '_urlEncode_iframe_';
            document.body.appendChild(form);
            return form;
        }

        function getIframe() {
            //隐藏iframe截获提交的字符串
            const iframe = document.createElement('iframe');
            //iframe.name = '_urlEncode_iframe_';
            iframe.setAttribute('name', '_urlEncode_iframe_');
            iframe.style.display = 'none';
            iframe.width = "0";
            iframe.height = "0";
            iframe.scrolling = "no";
            iframe.allowtransparency = "true";
            iframe.frameborder = "0";
            iframe.src = 'about:blank';
            return iframe;
        }

        function messageCb(event) {
            const origin = event.origin;
            if (origin !== 'https://livelazily.tk') {
                return;
            }
            const encodeStr = event.data;
            callback(encodeStr);

            form.parentNode.removeChild(form);
            iframe.parentNode.removeChild(iframe);
            window.removeEventListener('message', messageCb);
        }
    }

})(window.jQuery);
